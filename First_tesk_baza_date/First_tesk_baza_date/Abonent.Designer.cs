﻿namespace First_tesk_baza_date
{
    partial class Abonent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.name_TextBox = new System.Windows.Forms.TextBox();
            this.surname_TextBox = new System.Windows.Forms.TextBox();
            this.paitronymic_TextBox = new System.Windows.Forms.TextBox();
            this.adress_TextBox = new System.Windows.Forms.TextBox();
            this.comment_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.OK = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // name_TextBox
            // 
            this.name_TextBox.Location = new System.Drawing.Point(21, 12);
            this.name_TextBox.Name = "name_TextBox";
            this.name_TextBox.Size = new System.Drawing.Size(273, 26);
            this.name_TextBox.TabIndex = 0;
            // 
            // surname_TextBox
            // 
            this.surname_TextBox.Location = new System.Drawing.Point(21, 44);
            this.surname_TextBox.Name = "surname_TextBox";
            this.surname_TextBox.Size = new System.Drawing.Size(273, 26);
            this.surname_TextBox.TabIndex = 1;
            // 
            // paitronymic_TextBox
            // 
            this.paitronymic_TextBox.Location = new System.Drawing.Point(21, 76);
            this.paitronymic_TextBox.Name = "paitronymic_TextBox";
            this.paitronymic_TextBox.Size = new System.Drawing.Size(273, 26);
            this.paitronymic_TextBox.TabIndex = 2;
            // 
            // adress_TextBox
            // 
            this.adress_TextBox.Location = new System.Drawing.Point(21, 108);
            this.adress_TextBox.Name = "adress_TextBox";
            this.adress_TextBox.Size = new System.Drawing.Size(273, 26);
            this.adress_TextBox.TabIndex = 3;
            // 
            // comment_TextBox
            // 
            this.comment_TextBox.Location = new System.Drawing.Point(21, 140);
            this.comment_TextBox.Name = "comment_TextBox";
            this.comment_TextBox.Size = new System.Drawing.Size(273, 26);
            this.comment_TextBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(301, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Имя";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(301, 143);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Комментарий";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(301, 111);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Адрес";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(301, 79);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Отчество";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(301, 47);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Фамилия";
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(35, 172);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(109, 38);
            this.OK.TabIndex = 10;
            this.OK.Text = "Ок";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(150, 172);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(120, 38);
            this.Cancel.TabIndex = 11;
            this.Cancel.Text = "Отмена";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 222);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comment_TextBox);
            this.Controls.Add(this.adress_TextBox);
            this.Controls.Add(this.paitronymic_TextBox);
            this.Controls.Add(this.surname_TextBox);
            this.Controls.Add(this.name_TextBox);
            this.Name = "Form2";
            this.Text = "Абонент";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox name_TextBox;
        public System.Windows.Forms.TextBox surname_TextBox;
        public System.Windows.Forms.TextBox paitronymic_TextBox;
        public System.Windows.Forms.TextBox adress_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.Button Cancel;
        public System.Windows.Forms.TextBox comment_TextBox;
    }
}